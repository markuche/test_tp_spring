package org.formation.spring.test;

import org.formation.spring.config.ApplicationConfig;
import org.formation.spring.model.Adresse;
import org.formation.spring.model.Client;
import org.formation.spring.service.IPrestiBanqueService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test_TP5 {

	public static void main(String[] args) {
		
		
		
		ApplicationContext  context = new AnnotationConfigApplicationContext(ApplicationConfig.class);		   

		IPrestiBanqueService service = context.getBean("service", IPrestiBanqueService.class);

//		service.listClients();
//		service.addClient(new Client("Abdallah", "BOUMZAR", "login", "motDePasse",new Adresse(53, "rue", "ville") ));
//		service.addClient(new Client("Daniel", "BOUMZAr"," login", "motDePasse", new Adresse(24, "rue", "Paris")));
//		
//		service.deleteClient(3);
		Client c=service.editClient(2);
		c.setNom("Danielo");
		
//		service.updateClient(c);
		
		
		System.out.println(service.listClients());
		
		
	    ((ConfigurableApplicationContext)(context)).close();

	}

}
