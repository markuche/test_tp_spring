package org.formation.spring.test;

import static org.junit.Assert.*;

import org.formation.spring.config.ApplicationConfig;
import org.formation.spring.dao.CrudClientDAO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=ApplicationConfig.class)
public class TestUnitaire {

	@Autowired
	private CrudClientDAO crudClientDAO;
	
	@Test
	public void testDao() {
		assertNotNull(crudClientDAO);
		//assertTrue(crudClientDAO.findAllClientByNom("Abdallah");
	
		//assertEquals(crudClientDAO.findOne(2).getId(), 2);
		//assertEquals(crudClientDAO.findAllClientByNom("Abdallah").size(), 1L);
			assertEquals(crudClientDAO.findAllClientByLogin(" login").size(), 3L);
		
	}

	
	
	
	

}
